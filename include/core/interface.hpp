/*
 *  Author: ARDOUIN théo
 *  Create Time: 2019-09-23 15:56:53
 *  :------------:
 *  Modified by: Ardouin théo
 *  Modified time: 2019-09-25 00:59:00
 *  Description:
 */

#ifndef INTERFACE_HPP
    #define INTERFACE_HPP

    #include "Error.hpp"

    struct rect {
        int top;
        int left;
        int width;
        int heigh;
    };

    struct pos {
        int x;
        int y;
    };

    class Interface {
        public:
        Interface(struct rect _coord, Error (*_on_hover)(struct pos pos, void *args), Error (*_on_click)(struct pos pos, void *args)) {
            coords = _coord;
            on_hover = _on_hover;
            on_click = _on_click;
        };

        bool operator+(struct pos mouse) {
            if ((mouse.x > coords.top  && mouse.x <= coords.top  + coords.heigh) &&
                (mouse.y > coords.left && mouse.y <= coords.left + coords.width)) {
                    //effect on_hover
                    hovered = true;
                }
            hovered = false;
            return hovered;
        };

        private:
        struct rect coords;
        Error (*on_hover)(struct pos pos, void *args);
        Error (*on_click)(struct pos pos, void *args);
        bool hovered;
    };
#endif