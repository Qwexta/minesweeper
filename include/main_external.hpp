/*
 *  Author: ARDOUIN théo
 *  Create Time: 2019-09-23 11:12:03
 *  :------------:
 *  Modified by: Ardouin théo
 *  Modified time: 2019-09-23 11:18:59
 *  Description:
 */


#ifndef MAIN_EXTERNAL_HPP
    #define MAIN_EXTERNAL_HPP

    int main_external(int ac, const char *av[], const char *env[]);

#endif /* MAIN_EXTERNAL_HPP */