/*
 *  Author: ARDOUIN théo
 *  Create Time: 2019-10-11 11:04:09
 *  :------------:
 *  Modified by: Ardouin théo
 *  Modified time: 2019-10-17 16:54:57
 *  Description:
 */
#include <iostream>

#include "core/onEvent.hpp"
void redirect_view(Renderer *r, std::string name, __attribute__((unused))QXelements *elm) {
    std::cout << "redirect to " << name << std::endl;
    r->setView(name);
}

void filter(sf::Color color, QXelements *elm) {
    elm->_filter_color = color;
}
