project(QX)

list(APPEND SRCS
        ${CMAKE_CURRENT_SOURCE_DIR}/Renderer.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/View.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/elements/elements.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/elements/Button.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/onEvent.cpp
)

list(APPEND PUBLIC_HEADER
        ${CMAKE_CURRENT_SOURCE_DIR}/core/View.hpp
        ${CMAKE_CURRENT_SOURCE_DIR}/core/Renderer.hpp
        ${CMAKE_CURRENT_SOURCE_DIR}/core/maths.hpp
        ${CMAKE_CURRENT_SOURCE_DIR}/elements/elements.hpp
        ${CMAKE_CURRENT_SOURCE_DIR}/elements/Button.hpp
        ${CMAKE_CURRENT_SOURCE_DIR}/core/onEvent.hpp
)

set(SRCS "${SRCS}" PARENT_SCOPE)
set(PUBLIC_HEADER "${PUBLIC_HEADER}" PARENT_SCOPE)
#add_library(${PROJECT_NAME} ${QX_SRCS} ${QX_PUBLIC_HEADER})
