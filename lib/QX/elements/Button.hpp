/*
 *  Author: ARDOUIN théo
 *  Create Time: 2019-10-01 11:13:17
 *  :------------:
 *  Modified by: Ardouin théo
 *  Modified time: 2019-10-24 16:54:51
 *  Description:
 */

#ifndef QX_BUTTON_HPP
#define QX_BUTTON_HPP

#include <iostream>

#include "elements/elements.hpp"

class Button : public QXelements {
    public:
    Button(const char *name, sf::Sprite *sprite, sf::IntRect _rect);
    Button(const char *name, Animation *anim, sf::IntRect _rect);

    virtual void render(sf::RenderWindow *w, const sf::IntRect &parent_rect) const;

    sf::IntRect getRect() const { return _rect; };

    const std::string _name;
};

#endif