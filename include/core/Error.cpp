/*
 *  Author: ARDOUIN théo
 *  Create Time: 2019-09-25 11:36:50
 *  :------------:
 *  Modified by: Ardouin théo
 *  Modified time: 2019-10-09 15:12:28
 *  Description:
 */

#include "Error.hpp"


Error::Error() {
    validity = false;
}

Error::Error(char const *err) {
    if (err == nullptr) {
        validity = false;
        return;
    }
    msg = err;
    validity = true;
}

Error::~Error() {
}

Error::operator bool() {
    return validity;
}

bool Error::Valid() {
    return validity;
}

void Error::print() {
    std::cout << msg << "\n";
}
