/*
 *  Author: ARDOUIN théo
 *  Create Time: 2019-09-27 10:33:59
 *  :------------:
 *  Modified by: Ardouin théo
 *  Modified time: 2019-10-24 16:15:05
 *  Description:
 */

#include <iostream>
#include "core/maths.hpp"
#include "core/Renderer.hpp"

Renderer::Renderer(uint size_x, uint size_y, const std::string name)
    : _w(sf::VideoMode(size_x, size_y), name)
    , running(true)
    , s(sf::Vector2f(1, 1))
    , base_wsize(sf::Vector2u(size_x, size_y))
{
}

Renderer::~Renderer(void) {
    _w.close();
}

void Renderer::create_view(std::string name, QXelements *elm) {
    view[name] = elm;
}

void Renderer::render() {
    _w.clear();
    if (_current_view != "") {
        view[_current_view]->_render(&_w, sf::IntRect(0, 0, _w.getSize().x, _w.getSize().y));
    }
    _w.display();
}

void Renderer::process() {
    if (_current_view != "") {
        view[_current_view]->process();
    }
}

bool Renderer::setView(std::string v) {
    if (view.find(v) == view.end())
        return false;
    _current_view = v;
    return true;
}

std::string Renderer::getView() {
    return _current_view;
}

bool Renderer::event_handling(sf::Event &e) {
    if (!_w.pollEvent(e))
        return false;

    sf::Mouse m;
    const sf::Vector2i mpos = m.getPosition(_w);

    if (e.type == sf::Event::Closed) {
        running = false;
    }
    if (e.type == sf::Event::Resized) {
        s.x = e.size.width / (float)base_wsize.x;
        s.y = e.size.height / (float)base_wsize.y;
    }

    if (_current_view != "")
        view[_current_view]->event_handling(e, mpos, s);

    return true;
}