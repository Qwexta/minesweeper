/*
 *  Author: ARDOUIN théo
 *  Create Time: 2019-09-27 10:24:53
 *  :------------:
 *  Modified by: Ardouin théo
 *  Modified time: 2019-10-24 11:56:52
 *  Description:
 */

#ifndef RENDERER_HPP
#define RENDERER_HPP

#include <SFML/Graphics.hpp>
#include <string>
#include <map>

#include "core/View.hpp"

class Renderer {
    public:
    Renderer(uint size_x, uint size_y, const std::string name);
    ~Renderer(void);

    sf::RenderWindow _w;
    std::map<std::string, QXelements *> view;

    void create_view(std::string name, QXelements *elm);
    void render();
    void process();

    bool setView(std::string v);
    std::string getView();

    bool event_handling(sf::Event &e);

    bool running;
    private:
    std::string _current_view;
    sf::Vector2f s;
    const sf::Vector2u base_wsize;
};

#endif