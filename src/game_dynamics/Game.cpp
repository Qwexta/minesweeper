/*
 *  Author: ARDOUIN théo
 *  Create Time: 2019-09-25 11:34:49
 *  :------------:
 *  Modified by: Ardouin théo
 *  Modified time: 2019-09-27 10:00:17
 *  Description:
 */

#include "Game.hpp"

using namespace std;

Game::Game() {
    map = nullptr;
    x = 0;
    y = 0;
    bombs = 0;
}

void Game::display_debugging() {
    cout << "map size : x:" << x << " y: " << y << " bombs: " << bombs << "\n";
    for (int i = 0; i < x; i ++) {
        for (int j = 0; j < y; j ++) {
            if (!map[i][j].revealed)
                switch (map[i][j].type) {
                    case -1: cout << "B ";                                    break;
                    case  0: cout <<  "  ";                                   break;
                    case  1: cout <<  BLUE << "1 " << RESET;                  break;
                    case  2: cout <<  YELLOW << "2 " << RESET;                break;
                    case  3: cout <<  MAGENTA << "3 " << RESET;               break;
                    default: cout <<  RED << map[i][j].type << " " << RESET;
                }

            else
                cout << (int)map[i][j].type << " ";
        }
        cout << "\n";
    }
}