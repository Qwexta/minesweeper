/*
 *  Author: ARDOUIN théo
 *  Create Time: 2019-10-01 11:13:10
 *  :------------:
 *  Modified by: Ardouin théo
 *  Modified time: 2019-10-25 14:05:11
 *  Description:
 */

#include <iostream>

#include "Button.hpp"
#include "core/maths.hpp"
#include "core/onEvent.hpp"

Button::Button(const char *name, sf::Sprite *sprite, sf::IntRect rect) :
    _name(name)
{
    _is_animated = false;
    _rect = rect;
    _sprite = sprite;
}

Button::Button(const char *name, Animation *anim, sf::IntRect rect) :
    _name(name)
{
    _is_animated = true;
    _rect = rect;
    _anim = anim;
}

void Button::render(sf::RenderWindow *w, const sf::IntRect &parent_rect) const {
    if (_is_animated == false) {
        _sprite->setTextureRect(_rect);
        _sprite->setColor(_filter_color);
        _sprite->setPosition(sf::Vector2f(parent_rect.top + _rect.top,parent_rect.left + _rect.left));
        w->draw(*_sprite);
    }
}