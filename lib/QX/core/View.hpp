/*
 *  Author: ARDOUIN théo
 *  Create Time: 2019-09-27 14:06:34
 *  :------------:
 *  Modified by: Ardouin théo
 *  Modified time: 2019-10-24 14:42:11
 *  Description:
 */

#ifndef VIEW_HPP
#define VIEW_HPP

#include <vector>
#include <SFML/Graphics.hpp>

#include "elements/elements.hpp"

class View : public QXelements {
    public:
    View(sf::IntRect rect, sf::Color _bg_color = sf::Color(0, 0, 0));
    virtual void render(sf::RenderWindow *w, const __attribute__((unused)) sf::IntRect &parent_rect) const;

    private:
    sf::RectangleShape _rect_shape;
};

#endif
