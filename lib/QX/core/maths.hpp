/*
 *  Author: ARDOUIN théo
 *  Create Time: 2019-10-01 11:28:57
 *  :------------:
 *  Modified by: Ardouin théo
 *  Modified time: 2019-10-16 18:05:27
 *  Description:
 */

#ifndef QX_MATHS_HPP
#define QX_MATHS_HPP

#include <SFML/Graphics.hpp>

static inline bool inRect(sf::IntRect rect, sf::Vector2i m, sf::Vector2f s) {
    return m.x > rect.top * s.x && m.x < (rect.top + rect.height) * s.x && m.y > rect.left * s.y && m.y < (rect.left + rect.width) * s.y;
}

#endif