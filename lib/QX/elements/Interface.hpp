/*
 *  Author: ARDOUIN théo
 *  Create Time: 2019-10-01 11:48:21
 *  :------------:
 *  Modified by: Ardouin théo
 *  Modified time: 2019-10-01 11:50:26
 *  Description:
 */

#ifndef QX_INTERFACE_HPP
#define QX_INTERFACE_HPP

class Interface {
    public:
    
    private:
    void onHover();
    void onClick();
    void onRelease();
};

#endif