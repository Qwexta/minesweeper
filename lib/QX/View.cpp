/*
 *  Author: ARDOUIN théo
 *  Create Time: 2019-09-30 10:36:34
 *  :------------:
 *  Modified by: Ardouin théo
 *  Modified time: 2019-10-24 14:42:03
 *  Description:
 */
#include <iostream>

#include "elements/elements.hpp"
#include "core/View.hpp"

View::View(sf::IntRect rect, sf::Color bg_color)
{
    _rect = rect;
    _rect_shape = sf::RectangleShape(sf::Vector2f(rect.width, rect.height));
    _rect_shape.setPosition(sf::Vector2f(_rect.top, _rect.left));
    _rect_shape.setFillColor(bg_color);
}

void View::render(sf::RenderWindow *w, const __attribute__((unused)) sf::IntRect &parent_rect) const {
    w->draw(_rect_shape);
}
