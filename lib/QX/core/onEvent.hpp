/*
 *  Author: ARDOUIN théo
 *  Create Time: 2019-10-11 11:02:46
 *  :------------:
 *  Modified by: Ardouin théo
 *  Modified time: 2019-10-17 15:27:42
 *  Description:
 */

#ifndef ONEVENT_HPP
#define ONEVENT_HPP

#include "core/Renderer.hpp"

void redirect_view(Renderer *r, std::string name, __attribute__((unused))QXelements *elm);
void filter(sf::Color color, QXelements *elm);

#define FILTER_COLOR(color)             std::bind(filter, color, std::placeholders::_1)

#define REDIRECT_VIEW(r, view_name)     std::bind(redirect_view, r, std::string(view_name), std::placeholders::_1)

#endif