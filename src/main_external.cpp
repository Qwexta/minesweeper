/*
 *  Author: ARDOUIN théo
 *  Create Time: 2019-09-22 21:28:48
 *  :------------:
 *  Modified by: Ardouin théo
 *  Modified time: 2019-10-24 16:54:43
 *  Description: argument parcing and core luncher.
 */

#include <iostream>
#include "main_external.hpp"
#include "core/Error.hpp"
#include "Game.hpp"
#include "core/Renderer.hpp"

using namespace std;

#include "core/Renderer.hpp"
Renderer *init_graphicals_instance();

typedef struct {
    int resolution[2];
} args_t;

static Error parametre_handling(int ac, const char *av[], args_t *args) {
    if ( ac == 2 ) {
        return Error("unexpected parameters, please use ./{bin} --help\n");
    }
    return Error(nullptr);
}

int main_external(int ac, const char *av[], const char *env[]) {
    if ( env[0] == nullptr ) {
        cout << "fck off with your tty.";
        return EXIT_FAILURE;
    }

    args_t args;

    if ( Error err = parametre_handling(ac, av, &args) ) {
        err.print();
        return EXIT_FAILURE;
    }

    Game game = Game();
    if (Error err = game.generate_map()) {
        err.print();
        return EXIT_FAILURE;
    }

    //game.display_debugging();

    Renderer *r = init_graphicals_instance();

    sf::Event e;
    while(r->running) {
        while (r->event_handling(e)) { /* do something with event ?*/ }
        r->process();
        r->render();
    };
    return EXIT_SUCCESS;
}