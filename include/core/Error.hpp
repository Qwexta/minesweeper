/*
 *  Author: ARDOUIN théo
 *  Create Time: 2019-09-23 15:56:53
 *  :------------:
 *  Modified by: Ardouin théo
 *  Modified time: 2019-09-25 11:42:23
 *  Description:
 */

#ifndef ERROR_HPP
#define ERROR_HPP

#include <iostream>

class Error {
private:
    bool validity;
    char const *msg;

public:
    Error();
    Error(char const *err);
    ~Error();

    operator bool();

    bool Valid();
    void print();
};

#endif /* ERROR_HPP */