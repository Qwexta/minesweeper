/*
 *  Author: ARDOUIN théo
 *  Create Time: 2019-10-10 17:22:40
 *  :------------:
 *  Modified by: Ardouin théo
 *  Modified time: 2019-10-24 16:50:27
 *  Description:
 */

#include <iostream>
#include "elements/elements.hpp"
#include "core/maths.hpp"

QXelements::QXelements(void) {
    _on_hover   = nullptr;
    _on_release = nullptr;
    _on_click   = nullptr;
    _filter_color = sf::Color::White;
}

QXelements &QXelements::on_hover(callback_hover cb) {
    _on_hover = cb;
    return *this;
}

QXelements &QXelements::on_click(callback_click cb) {
    _on_click = cb;
    return *this;
}

QXelements &QXelements::on_release(callback_release cb) {
    _on_release = cb;
    return *this;
}

void QXelements::process() {
    for(std::size_t i=0; i != elms.size(); ++i)
        elms[i]->process();

    switch(state) {
        case QXelements::State::In: if (_on_hover) _on_hover(this); break;
        case QXelements::State::Click: if (_on_click) _on_click(this); break;
        case QXelements::State::Release: if (_on_release) _on_release(this); state = QXelements::State::In; break;
        case QXelements::State::Out:
            _filter_color = sf::Color::White;
        break;
    }
}

QXelements &QXelements::add_elements(QXelements *e) {
    elms.push_back(e);
    return *this;
}

static void even_state_setter(QXelements *elm, QXelements::State e) {
    switch(e) {
        case QXelements::State::Click:   if (elm->state < 3) elm->state = e; break;
        case QXelements::State::In:      if (elm->state < 2) elm->state = e; break;
        case QXelements::State::Release: elm->state = e;                     break;
        case QXelements::State::Out:     elm->state = e;                     break;
    }
}

void QXelements::event_handling(const sf::Event &e, const sf::Vector2i &mpos, const sf::Vector2f &s) {
    if (inRect(this->_rect, mpos, s)) {
        switch (e.type) {
            case sf::Event::MouseButtonReleased: even_state_setter(this, QXelements::State::Release); break;
            case sf::Event::MouseButtonPressed:  even_state_setter(this, QXelements::State::Click); break;
            default: if (this->state < QXelements::State::Click) even_state_setter(this, QXelements::State::In); break;
        }
    } else {
        even_state_setter(this, QXelements::State::Out);
    }
    for(std::size_t i=0; i != elms.size(); ++i) {
        elms[i]->event_handling(e, mpos, s);
    }
}

void QXelements::_render(sf::RenderWindow *w, const sf::IntRect &parent_rect) {
    render(w, parent_rect);
    for(std::size_t i=0; i != elms.size(); ++i) {
        elms[i]->_render(w, _rect);
    }
}