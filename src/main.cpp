/*
 *  Author: ARDOUIN théo
 *  Create Time: 2019-09-22 21:28:48
 *  :------------:
 *  Modified by: Ardouin théo
 *  Modified time: 2019-09-23 11:12:24
 *  Description: argument parcing and core luncher.
 */

#include "main_external.hpp"

int main(int ac, const char *av[], const char *env[]) {
    return main_external(ac, av, env);
}