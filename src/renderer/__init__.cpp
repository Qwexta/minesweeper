/*
 *  Author: ARDOUIN théo
 *  Create Time: 2019-09-27 10:20:51
 *  :------------:
 *  Modified by: Ardouin théo
 *  Modified time: 2019-10-24 16:50:49
 *  Description:
 */
#include <iostream>
#include <cstdlib>

#include "core/Renderer.hpp"
#include "core/View.hpp"
#include "elements/Button.hpp"
#include "core/onEvent.hpp"

Renderer *init_graphicals_instance() {
    Renderer *renderer = new Renderer(800, 600, "minesweeper");

    sf::Texture *texture = new sf::Texture;
    if (!texture->loadFromFile("./image.png")) {
        std::cout << "image not loaded \n";
    }
    sf::Sprite *sprite = new sf::Sprite;
    sprite->setTexture(*texture);

    renderer->create_view("game", new View(sf::IntRect(0, 0, 100, 100)));
    renderer->create_view("menu", new View(sf::IntRect(0, 0, 200, 100)));

    renderer->view["menu"]->add_elements(
        &(
            new Button ( "start_game", sprite, sf::IntRect(0, 0, 200, 200) )
        )
        ->on_hover( FILTER_COLOR(sf::Color::Blue) )
        .on_release( REDIRECT_VIEW(renderer, "game") )
        .add_elements (
                    &(
                        new Button  ( "start_game", sprite, sf::IntRect(0, 0, 30, 30) )
                    )
                    ->on_hover( FILTER_COLOR(sf::Color::Black) )
                )
    );

    renderer->view["menu"]->add_elements(
        &(
            new Button ("Quit", sprite, sf::IntRect(300, 300, 100, 100) )
        )
        ->on_hover( FILTER_COLOR(sf::Color(32, 200, 0)) )
        .on_click( FILTER_COLOR(sf::Color::Red)) );

    renderer->view["game"]->add_elements(
        &(
            new Button("Quit", sprite, sf::IntRect(500, 200, 120, 80))
        )
        ->on_hover(FILTER_COLOR(sf::Color::Yellow)));

    renderer->setView("menu");
    return renderer;
}