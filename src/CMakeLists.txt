project(source)

list(APPEND SRCS
        ${CMAKE_CURRENT_SOURCE_DIR}/main.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/main_external.cpp
)

add_subdirectory(user_interaction_handler)
add_subdirectory(renderer)
add_subdirectory(game_dynamics)

set(SRCS "${SRCS}" PARENT_SCOPE)