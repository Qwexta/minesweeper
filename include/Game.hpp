/*
 *  Author: ARDOUIN théo
 *  Create Time: 2019-09-25 11:04:10
 *  :------------:
 *  Modified by: Ardouin théo
 *  Modified time: 2019-09-26 16:11:46
 *  Description:
 */

#ifndef GAME_HPP
#define GAME_HPP

#include <cstdlib>
#include <ctime>
#include <cstring>
#include "core/Error.hpp"

const char RED[]     = "\033[1;31m";
const char BLUE[]    = "\033[0;36m";
const char MAGENTA[] = "\033[0;35m";
const char YELLOW[]  = "\033[0;33m";
const char RESET[]   = "\033[0;m";

typedef struct tile {
    short int type;
    bool revealed;
} tile_t;

class Game {
private:
    tile_t **map;
    int x;
    int y;
    int bombs;

public:
    Game();

    Error generate_map();
    Error generate_map(int x, int y, int bombs);
    int   count_bombs(int x, int y);
    void  display_debugging();
    void  display_map();
};

#endif