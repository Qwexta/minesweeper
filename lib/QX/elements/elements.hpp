/*
 *  Author: ARDOUIN théo
 *  Create Time: 2019-10-10 10:33:55
 *  :------------:
 *  Modified by: Ardouin théo
 *  Modified time: 2019-10-24 14:41:53
 *  Description:
 */

#ifndef ELEMENTS_HPP
#define ELEMENTS_HPP

#include <functional>
#include <SFML/Graphics.hpp>
#include <string>

#include "core/Animation.hpp"

class QXelements {
    public:

    QXelements();
    using callback_hover=std::function<void(QXelements *b)>;
    using callback_click=std::function<void(QXelements *b)>;
    using callback_release=std::function<void(QXelements *b)>;

    enum State {
        In      = 1,
        Click   = 2,
        Release = 3,
        Out     = 0,
        None    = -1,
    };

    QXelements &on_hover(callback_hover);
    QXelements &on_click(callback_click);
    QXelements &on_release(callback_release);

    sf::Sprite *_sprite;
    sf::IntRect _rect;
    sf::Color _filter_color;

    bool        _is_animated;
    Animation  *_anim;

    virtual void render(sf::RenderWindow *w, const sf::IntRect &parent_rect) const = 0;

    void process();
    virtual ~QXelements() {};
    QXelements &add_elements(QXelements *e);
    void event_handling(const sf::Event &e, const sf::Vector2i &mpos, const sf::Vector2f &s);
    void _render(sf::RenderWindow *w, const sf::IntRect &parent_rect);

    callback_hover _on_hover;
    callback_click _on_click;
    callback_release _on_release;

    State state;
    std::vector<QXelements *> elms;
};

void filter(sf::Color color, QXelements *b);
#endif