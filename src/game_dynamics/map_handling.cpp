/*
 *  Author: ARDOUIN théo
 *  Create Time: 2019-09-26 15:35:46
 *  :------------:
 *  Modified by: Ardouin théo
 *  Modified time: 2019-09-27 10:01:23
 *  Description:
 */

#include "Game.hpp"

using namespace std;

Error Game::generate_map() {
    return generate_map( 50, 50, 320 );
}

Error Game::generate_map(int _x, int _y, int _bombs) {
    x = _x;
    y = _y;
    bombs = _bombs;

    if (bombs >  x * y) {
        return Error("bombs's number exceed map size.");
    }
    map = (tile_t **)malloc(sizeof(tile_t *) * y);
    if (map == nullptr) {
        return Error("allocation_failed.");
    }
    for (int i = 0; i < y; i++) {
        map[i] = (tile_t *)malloc(sizeof(tile_t) * x);
        if (map == nullptr) {
            return Error("allocation_failed.");
        }
        memset(map[i], 0, sizeof(tile_t) * x);
    }

    int proportion = x*y / bombs;
    int placed = 0;
    srand(time(nullptr) + proportion);

    for (int ix= 0; placed < bombs && ix < x; ix++) {
        for (int iy = 0; placed < bombs && iy < x; iy++) {
            if (proportion < static_cast <float> (rand()) / static_cast <float> (RAND_MAX)) {
                map[ix][iy].type = -1;
                placed++;
            }
        }
    }

    while (placed < bombs) {
        int rx = rand() % x;
        int ry = rand() % y;
        if (map[rx][ry].type != -1) {
            map[rx][ry].type = -1;
            placed += 1;
        }
    }

    for (int ix = 0; ix < x; ix++) {
        for (int iy = 0; iy < y; iy++) {
            if (map[ix][iy].type != -1) {
                map[ix][iy].type = count_bombs(ix, iy);
            }
        }
    }

    return Error(nullptr);
}

int Game::count_bombs(int ix, int iy) {
    int count = 0;
    if (ix > 0 && map[ix - 1][iy].type == -1)                     count += 1;
    if (ix < x - 1 && map[ix + 1][iy].type == -1)                 count += 1;
    if (iy > 0 && map[ix][iy - 1].type == -1)                     count += 1;
    if (iy < y - 1 && map[ix][iy + 1].type == -1)                 count += 1;
    if (ix > 0 && iy > 0 && map[ix - 1][iy - 1].type == -1)       count += 1;
    if (ix > 0 && iy < y - 1&& map[ix - 1][iy + 1].type == -1)    count += 1;
    if (ix < x - 1&& iy > 0 && map[ix + 1][iy - 1].type == -1)    count += 1;
    if (ix < x - 1&& iy < y - 1&& map[ix + 1][iy + 1].type == -1) count += 1;
    return count;
}

void Game::display_map() {
    for (int i = 0; i < x; i ++) {
        for (int j = 0; j < y; j ++) {
            if (map[i][j].type == -1)
                cout <<  RED << "B " << RESET;
            else
                cout << (int)map[i][j].type << " ";
        }
        cout << "\n";
    }
}